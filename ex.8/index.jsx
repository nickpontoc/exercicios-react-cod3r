import React from 'react'
import ReactDOM from 'react-dom'
import Family from './family'
import Member from './member';

ReactDOM.render(
    <div>
        <Family  lastName="Fonseca">
            <Member name="Nicholas"/>
        </Family>
    </div>
, document.getElementById('app'))
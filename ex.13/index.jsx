import React from 'react'
import ReactDOM from 'react-dom'
import { combineReducers, createStore} from 'redux'
import { Provider} from 'react-redux'

import Field from './field'
import fieldreducer from './fieldReducer'
const reducers = combineReducers({
    field: fieldreducer
})

ReactDOM.render(
    <Provider store={createStore(reducers)}>
        <Field initialValue="teste" />
    </Provider>
    , document.getElementById('app'))
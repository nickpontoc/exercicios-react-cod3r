import React from 'react'
import ReactDOM from 'react-dom'
import Family from './family'
import Member from './member';

ReactDOM.render(
    <div>
        <Family>
            <Member name="Nicholas" lastName="Fonseca" />
        </Family>
    </div>
, document.getElementById('app'))